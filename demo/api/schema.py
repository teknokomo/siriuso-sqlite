import random

from django.utils import timezone
from graphene_django import DjangoObjectType
from graphene_django.filter import DjangoFilterConnectionField
import graphene

from ..models import *


class SequenceNode(DjangoObjectType):
    class Meta:
        model = Header
        filter_fields = {
            'uuid': ['exact'],
            'creation_date': ['exact', 'gte', 'gt', 'lte', 'lt'],
            'name': ['exact', 'istartswith', 'iendswith']
        }
        interfaces = (graphene.relay.Node,)


class SequenceLineNode(DjangoObjectType):
    class Meta:
        model = Line
        filter_fields = {
            'header': ['exact'],
            'creation_date': ['exact', 'gte', 'gt', 'lte', 'lt']
        }
        interfaces = (graphene.relay.Node,)


class RandomPoint(graphene.ObjectType):
    point = graphene.Int()
    time = graphene.DateTime()

    @staticmethod
    def resolve_point(root, info, **kwargs):
        return random.randint(-10000, 10000)

    @staticmethod
    def resolve_time(root, info, **kwargs):
        return timezone.now()


class DemoQuery(graphene.ObjectType):
    sequences = DjangoFilterConnectionField(SequenceNode)
    sequence_lines = DjangoFilterConnectionField(SequenceLineNode)
    random_point = graphene.Field(RandomPoint)

    @staticmethod
    def resolve_random_point(root, info, **kwargs):
        return RandomPoint()
