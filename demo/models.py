import string
import random

from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator
from uuid import uuid4


# Create your models here.
class Header(models.Model):
    uuid = models.UUIDField('UUID', primary_key=True, default=uuid4)

    creation_date = models.DateTimeField('Дата создания', auto_now_add=True, db_index=True)

    name = models.TextField('Название последовательности', max_length=1024, unique=True)

    point_count = models.BigIntegerField('Количество точек последовательности', default=5000, blank=False,
                                         null=False, validators=(MinValueValidator(1), MaxValueValidator(10000)))

    min_val = models.BigIntegerField('Нижний порог значений', default=-10000, blank=False, null=False,
                                     validators=(MinValueValidator(-10000), MaxValueValidator(10000)))

    max_val = models.BigIntegerField('Верхний порог значений', default=10000, blank=False, null=False,
                                     validators=(MinValueValidator(-10000), MaxValueValidator(10000)))

    completed = models.BooleanField('Последовательность завершена', default=False)

    class Meta:
        db_table = 'demo_headers'
        verbose_name = 'Последовательность'
        verbose_name_plural = 'Последовательности'


class Line(models.Model):
    uuid = models.UUIDField('UUID', primary_key=True, default=uuid4)

    creation_date = models.DateTimeField('Дата создания', auto_now_add=True, db_index=True)

    header = models.ForeignKey(Header, on_delete=models.CASCADE, null=False, blank=False)

    values_json = models.TextField('JSON значений', max_length=10000000, null=True, blank=True)

    values_count = models.BigIntegerField('Количество значений', null=False, blank=False, default=0,
                                          validators=(MinValueValidator(0), MaxValueValidator(10000)))

    class Meta:
        db_table = 'demo_lines'
        verbose_name = 'Строка последовательности'
        verbose_name_plural = 'Строки последовательностей'


def gen_name(instance, filename):
    letters = string.ascii_letters + string.digits
    rnd_string = ''.join(random.choice(letters) for i in range(10))
    # указание папки для хранения файлов и собирание конечного названия с сохранением изначального расширения
    return 'charts_data/{0}.{1}'.format(rnd_string, filename.split('.')[-1])


class File(models.Model):
    uuid = models.UUIDField('UUID', primary_key=True, default=uuid4)

    creation_date = models.DateTimeField('Дата создания', auto_now_add=True, db_index=True)

    file = models.FileField('Сгенерированный файл', upload_to=gen_name)
