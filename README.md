# siriuso-sqlite

Бэкенд с использованием компактной встраиваемой СУБД SQLite

Как запустить:
1. Установить Python 3.8 или выше

2. Внутри виртуального окружения выполнить установку всех необходимых библиотек:<br>
pip install -r requirements.txt

3. Применить изменения в базе данных:<br> 
python manage.py migration

4. Запустить backend:<br>
python manage.py runserver
